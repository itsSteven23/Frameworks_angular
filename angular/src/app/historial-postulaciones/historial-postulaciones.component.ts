// historial-postulaciones.component.ts
import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';
import { Router } from '@angular/router'; // Agrega esta importación

@Component({
  selector: 'app-historial-postulaciones',
  templateUrl: './historial-postulaciones.component.html',
  styleUrls: ['./historial-postulaciones.component.css']
})
export class HistorialPostulacionesComponent implements OnInit {
  postulaciones: any[] = [];

  constructor(
    private localStorageService: LocalStorageService,
    private router: Router // Inyecta el servicio Router
  ) {}

  ngOnInit(): void {
    this.mostrarPostulaciones();
  }

  mostrarPostulaciones() {
    // Obtener las postulaciones del almacenamiento local
    const postulacionesGuardadas = this.localStorageService.getItem('postulaciones') || [];

    // Verificar si hay postulaciones
    if (postulacionesGuardadas.length > 0) {
      this.postulaciones = postulacionesGuardadas;
    }
  }

  eliminarPostulacion(index: number) {
    // Obtener las postulaciones del almacenamiento local
    const postulacionesGuardadas = this.localStorageService.getItem('postulaciones') || [];

    // Eliminar la postulación del arreglo
    const [postulacionEliminada] = postulacionesGuardadas.splice(index, 1);

    // Guardar las postulaciones actualizadas en el almacenamiento local
    this.localStorageService.setItem('postulaciones', postulacionesGuardadas);

    // Volver a mostrar las postulaciones en el historial
    this.mostrarPostulaciones();

    // Notificar al usuario (puedes usar algún servicio para mostrar notificaciones)
    alert(`Postulación para ${postulacionEliminada.titulo} en ${postulacionEliminada.empresa} eliminada.`);
  }

  cerrarSesion() {
    // Redireccionar a la página de inicio de sesión
    this.router.navigate(['/login']); // Redirige al componente de inicio de sesión
  }
}
