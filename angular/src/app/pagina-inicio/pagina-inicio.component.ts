import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagina-inicio',
  templateUrl: './pagina-inicio.component.html',
  styleUrls: ['./pagina-inicio.component.css'] // Verifica esta ruta
})
export class PaginaInicioComponent {
  constructor(private router: Router) {}

  cerrarSesion() {
    // Redireccionar a la página de inicio de sesión
    this.router.navigate(['/login']); // Redirige al componente de inicio de sesión
  }
}
