// empresas-colaboradoras.component.ts
import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-empresas-colaboradoras',
  templateUrl: './empresas-colaboradoras.component.html',
  styleUrls: ['./empresas-colaboradoras.component.css']
})
export class EmpresasColaboradorasComponent implements OnInit {
  empresasColaboradoras = [
    { titulo: 'Analista de Datos', empresa: 'Empresa D', ubicacion: 'Cuenca', contacto: '0966-111-222', correo: 'empresaD@example.com' },
    { titulo: 'Especialista en Marketing Digital', empresa: 'Empresa E', ubicacion: 'Manta', contacto: '0955-333-444', correo: 'empresaE@example.com' },
    { titulo: 'Ingeniero Civil', empresa: 'Empresa F', ubicacion: 'Ambato', contacto: '0944-555-666', correo: 'empresaF@example.com' },
    { titulo: 'Diseñador Gráfico', empresa: 'Empresa G', ubicacion: 'Loja', contacto: '0933-777-888', correo: 'empresaG@example.com' },
    { titulo: 'Contador Financiero', empresa: 'Empresa H', ubicacion: 'Esmeraldas', contacto: '0922-999-000', correo: 'empresaH@example.com' },
    // Agrega más empresas colaboradoras según sea necesario
  ];

  constructor(private localStorageService: LocalStorageService) { }

  ngOnInit(): void {
  }

  postularse(titulo: string, empresa: string): void {
    // Implementa lógica de postulación aquí

    // Agregar la postulación al historial
    const postulacionesGuardadas = this.localStorageService.getItem('postulaciones') || [];
    postulacionesGuardadas.push({ titulo, empresa });
    localStorage.setItem('postulaciones', JSON.stringify(postulacionesGuardadas));

    alert(`Te has postulado para el puesto de ${titulo} en ${empresa}. ¡Buena suerte!`);
  }
}
