// auth.service.ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private isAuthenticated: boolean = false;

  login(username: string, password: string): boolean {
    // Aquí puedes agregar la lógica de autenticación
    // Por ahora, simplemente devolvemos true si el usuario y la contraseña no están vacíos
    this.isAuthenticated = username !== '' && password !== '';
    return this.isAuthenticated;
  }

  logout(): void {
    // Aquí puedes agregar la lógica de cierre de sesión
    this.isAuthenticated = false;
  }

  isLoggedIn(): boolean {
    return this.isAuthenticated;
  }
}
