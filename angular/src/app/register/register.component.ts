import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  ngOnInit() {
    console.log('Script de registro cargado correctamente.');
  }

  onSubmit() {
    // Obtener valores del formulario
    const email = (document.getElementById('email') as HTMLInputElement).value;
    const password = (document.getElementById('password') as HTMLInputElement).value;

    // Crear un objeto con los datos del usuario
    const userData = {
      email: email.trim(),
      password: password.trim(),
      // Agregar otros campos según necesites
    };

    // Intentar convertir a cadena JSON y almacenar en el Local Storage
    try {
      localStorage.setItem('userData', JSON.stringify(userData));
      console.log('Usuario registrado:', userData);

      alert('Registro exitoso. Ahora puedes iniciar sesión.');
      // Redirigir al usuario a la página de inicio de sesión
      window.location.href = 'index.html';
    } catch (error) {
      console.error('Error al guardar en Local Storage:', error);
      alert('Hubo un problema al registrar. Por favor, inténtalo de nuevo.');
    }
  }
}
