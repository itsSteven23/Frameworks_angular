import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { PaginaInicioComponent } from './pagina-inicio/pagina-inicio.component';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { EmpresasColaboradorasComponent } from './empresas-colaboradoras/empresas-colaboradoras.component';
import { EventosTalleresComponent } from './eventos-talleres/eventos-talleres.component';
import { OfertasExclusivasComponent } from './ofertas-exclusivas/ofertas-exclusivas.component';
import { HistorialPostulacionesComponent } from './historial-postulaciones/historial-postulaciones.component';  // Agrega esta línea
import { RedesSocialesComponent } from './redes-sociales/redes-sociales.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'paginainicio', component: PaginaInicioComponent },
  { path: 'busqueda', component: BusquedaComponent },
  { path: 'empresas', component: EmpresasColaboradorasComponent },
  { path: 'eventos', component: EventosTalleresComponent },
  { path: 'ofertas', component: OfertasExclusivasComponent },
  { path: 'historial', component: HistorialPostulacionesComponent },
  { path: 'redessocialesycontacto', component: RedesSocialesComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

