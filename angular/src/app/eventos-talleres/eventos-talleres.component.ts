// eventos-talleres.component.ts
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-eventos-talleres',
  templateUrl: './eventos-talleres.component.html',
  styleUrls: ['./eventos-talleres.component.css']
})
export class EventosTalleresComponent implements OnInit {
  eventosTalleres: any[] = [];

  ngOnInit() {
    // Simulación de eventos y talleres (en un entorno real, esto vendría del servidor)
    this.eventosTalleres = [
      { nombre: 'Taller de Desarrollo Profesional', fecha: '2023-11-01', ubicacion: 'Salón 101' },
      { nombre: 'Conferencia sobre Tecnologías Emergentes', fecha: '2023-11-15', ubicacion: 'Auditorio Principal' },
      // Agrega más eventos y talleres simulados según sea necesario
    ];

    console.log(this.eventosTalleres); // Verifica si los datos se están cargando correctamente
  }
}
