import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventosTalleresComponent } from './eventos-talleres.component';

describe('EventosTalleresComponent', () => {
  let component: EventosTalleresComponent;
  let fixture: ComponentFixture<EventosTalleresComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EventosTalleresComponent]
    });
    fixture = TestBed.createComponent(EventosTalleresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
