import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { PaginaInicioComponent } from './pagina-inicio/pagina-inicio.component';
import { RouterModule } from '@angular/router';
import { BusquedaComponent } from './busqueda/busqueda.component';
import { EmpresasColaboradorasComponent } from './empresas-colaboradoras/empresas-colaboradoras.component';
import { EventosTalleresComponent } from './eventos-talleres/eventos-talleres.component';
import { OfertasExclusivasComponent } from './ofertas-exclusivas/ofertas-exclusivas.component';
import { HistorialPostulacionesComponent } from './historial-postulaciones/historial-postulaciones.component';
import { RedesSocialesComponent } from './redes-sociales/redes-sociales.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    DashboardComponent,
    DashboardComponentComponent,
    PaginaInicioComponent,
    BusquedaComponent,
    EmpresasColaboradorasComponent,
    EventosTalleresComponent,
    OfertasExclusivasComponent,
    HistorialPostulacionesComponent,
    RedesSocialesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
