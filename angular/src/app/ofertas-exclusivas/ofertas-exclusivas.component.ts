// ofertas-exclusivas.component.ts
import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-ofertas-exclusivas',
  templateUrl: './ofertas-exclusivas.component.html',
  styleUrls: ['./ofertas-exclusivas.component.css']
})
export class OfertasExclusivasComponent implements OnInit {
  ofertasExclusivas: any[] = [];

  constructor(private localStorageService: LocalStorageService) {}

  ngOnInit() {
    // Simulación de ofertas exclusivas (en un entorno real, esto vendría del servidor)
    this.ofertasExclusivas = [
      { 
        titulo: 'Desarrollador Frontend', 
        empresa: 'Empresa C', 
        ubicacion: 'Cuenca', 
        contacto: '0999-123-456', 
        correo: 'contacto@empresaC.com' 
      },
      { 
        titulo: 'Diseñador UX/UI', 
        empresa: 'Empresa D', 
        ubicacion: 'Manta', 
        contacto: '0988-234-567', 
        correo: 'contacto@empresaD.com' 
      },
      // Agrega más ofertas exclusivas simuladas según sea necesario
    ];
  }

  postularse(titulo: string, empresa: string) {
    // Agregar la postulación al historial
    const postulacionesGuardadas = this.localStorageService.getItem('postulaciones') || [];
    postulacionesGuardadas.push({ titulo, empresa });
    this.localStorageService.setItem('postulaciones', postulacionesGuardadas);

    // Mostrar notificación
    alert(`Te has postulado para el puesto de ${titulo} en ${empresa}. ¡Buena suerte!`);
  }
}
