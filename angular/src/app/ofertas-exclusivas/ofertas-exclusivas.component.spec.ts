import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfertasExclusivasComponent } from './ofertas-exclusivas.component';

describe('OfertasExclusivasComponent', () => {
  let component: OfertasExclusivasComponent;
  let fixture: ComponentFixture<OfertasExclusivasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OfertasExclusivasComponent]
    });
    fixture = TestBed.createComponent(OfertasExclusivasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
