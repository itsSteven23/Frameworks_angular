// redes-sociales.component.ts
import { Component } from '@angular/core';

@Component({
  selector: 'app-redes-sociales',
  templateUrl: './redes-sociales.component.html',
  styleUrls: ['./redes-sociales.component.css']
})
export class RedesSocialesComponent {

  redesYContactoSimulados = [
    { tipo: 'Facebook', enlace: 'https://www.facebook.com/bolsaempleouleam/' },
    { tipo: 'Twitter', enlace: 'https://twitter.com/bolsaempleouleam' },
    { tipo: 'LinkedIn', enlace: 'https://www.linkedin.com/company/bolsaempleouleam' },
    { tipo: 'Correo Electrónico', enlace: 'mailto:info@bolsaempleouleam.com' }
    // Agrega más enlaces simulados según sea necesario
  ];

  constructor() { }

}
