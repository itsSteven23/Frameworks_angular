// login.component.ts

import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string = '';
  password: string = '';

  constructor(private router: Router) {}

  login() {
    // Validar las credenciales (esto debería hacerse en el servidor en un entorno de producción)
    if (this.email === 'usuario@example.com' && this.password === 'contraseña') {
      alert('Inicio de sesión exitoso');
      // Redirigir al usuario a la página principal
      this.router.navigate(['/paginainicio']);
    } else {
      alert('Credenciales incorrectas. Intenta de nuevo.');
    }
    
  }
  goToRegister() {
    // Navegar a la página de registro usando la ruta
    this.router.navigateByUrl('/register');
  }
}
